// Configuration
const CONFIG = require('../globals.config.json')
// App version
const APP_VERSION = CONFIG.VERSION.CURRENT

;(async () => {
  // Start
  console.log('Post-build script: started...\n')

  console.log('Build finish for version: ' + APP_VERSION + '\n')

  // End
  console.log('Post-build script: exit')
})()
