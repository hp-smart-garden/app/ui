# 🍃 *Smart Garden App* (UI) 🌐

[![smart-garden](https://img.shields.io/static/v1?label=&message=<%= encodeURIComponent(GLOBALS.APP_NAME) %>&color=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&logo=<%= logoImageUrl %>)](https://gitlab.com/hperchec/smart-garden)
[![app-ui](https://img.shields.io/static/v1?labelColor=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&label=ui&message=v<%= GLOBALS.VERSION.CURRENT %>&color=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8I////ev///+n/////////////////////////6v///+r/////////////////////////6f///3r///8I////Sf////D/////////////////////////zv///zX///81////zv/////////////////////////w////Sf///2f////8/////////////////////////9D///82////Nv///9D//////////////////////////P///2f///9n/////P/////////r////4P///+D////g////zv///87////g////4P///+D////r//////////z///9n////Z/////z////+////bv///xv///8f////H////yD///8g////H////x////8b////bv////7////8////Z////2f////8/////v///1n///8A////AP///wD///8A////AP///wD///8A////AP///1n////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9Z////AP///wD///8A////AP///wD///8A////AP///wD///9Z/////v////z///9n////Z/////3////+////bv///xv///8f////H////x////8f////H////x////8b////bv////7////9////Z////0n////w/////////+v////g////4P///+D////g////4P///+D////g////4P///+v/////////8P///0n///8I////ev///+n/////////////////////////////////////////////////////////6f///3r///8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB+AAAAfgAAAH4AAAB+AAAAfgAAAH4AAAAAAAAAAAAAAAAAAAAAAAAA==)](<%= projectUrl %>)
[![scorpion](https://img.shields.io/static/v1?label=&message=Scorpion&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Vuejs](https://shields.io/static/v1?logo=vuedotjs&label=&labelColor=5B5B5B&message=Vue.js&color=5B5B5B)](https://vuejs.org/)
[![Webpack](https://shields.io/static/v1?logo=webpack&label=&labelColor=5B5B5B&message=Webpack&color=5B5B5B)](https://webpack.js.org/)
[![Cordova](https://shields.io/static/v1?logo=apachecordova&label=&labelColor=5B5B5B&message=Cordova&color=5B5B5B)](https://cordova.apache.org/)
[![pipeline-status](<%= projectUrl %>/badges/main/pipeline.svg)](<%= projectUrl %>/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

> This project is based on [Scorpion](https://gitlab.com/hperchec/boilerplates/scorpion/app/ui) boilerplate **ui** project

**Table of contents**:

[[_TOC_]]

## Get started

> **IMPORTANT**: Please refer to the [project documentation](https://gitlab.com/hperchec/smart-garden/docs).

Clone this repository:

```bash
git clone <%= repositoryUrl %>
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)