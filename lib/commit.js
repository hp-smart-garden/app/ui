const childp = require('child_process')
const colors = require('colors')

module.exports = function (msg) {
  // Start
  console.log('- Script: commit - started ▶'.cyan)
  console.log('-----------------------------\n'.cyan)

  console.log(`Commit "${msg}"\n`)
  childp.execSync(
    `git add .`
  )
  childp.execSync(
    `git commit -m "${msg}"`,
    {stdio: 'inherit'}
  )

  // End
  console.log('----\n'.cyan)
}
