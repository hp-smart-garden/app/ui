// Vue-tailwind component
import { TButton } from 'vue-tailwind/dist/components'

export default {
  component: TButton,
  props: {
    // Fixed
    fixedClasses: 'transition duration-200 ease-in-out disabled:pointer-events-none',
    // Default
    classes: 'text-dark bg-primary border border-transparent rounded-full px-8 py-2 hover:bg-primary-400 hover:shadow focus:shadow disabled:text-gray-400 disabled:bg-primary-200 dark:disabled:bg-dark-2',
    // Variants
    variants: {
      primary: 'text-dark bg-primary border border-transparent rounded-full px-8 py-2 hover:bg-primary-400 hover:shadow focus:shadow disabled:text-gray-400 disabled:bg-primary-200 dark:disabled:bg-dark-2',
      success: 'text-dark bg-success border border-transparent rounded-full px-8 py-2 hover:bg-success-400 hover:shadow focus:shadow disabled:text-gray-400 disabled:bg-success-400 dark:disabled:bg-dark-2',
      error: 'text-dark bg-error border border-transparent rounded-full px-8 py-2 hover:bg-error-400 hover:shadow focus:shadow disabled:text-gray-200 dark:disabled:text-gray-400 disabled:bg-error-100 dark:disabled:bg-dark-2',
      warning: 'text-dark bg-warning border border-transparent rounded-full px-8 py-2 hover:bg-warning-400 hover:shadow focus:shadow disabled:text-gray-400 disabled:bg-warning-400 dark:disabled:bg-dark-2'
    }
  }
}
