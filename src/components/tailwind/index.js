import TAlert from './TAlert'
import TButton from './TButton'
import TDropdown from './TDropdown'
import TDropdownButton from './TDropdownButton'
import TInput from './TInput'
import TMenuButton from './TMenuButton'
import TModal from './TModal'
import TRadio from './TRadio'
import TSelect from './TSelect'
import TRichSelect from './TRichSelect'

// Tailwind components settings
export const VTWSettings = {
  't-alert': TAlert,
  't-button': TButton,
  't-dropdown': TDropdown,
  't-dropdown-button': TDropdownButton,
  't-input': TInput,
  't-menu-button': TMenuButton,
  't-modal': TModal,
  't-radio': TRadio,
  't-select': TSelect,
  't-rich-select': TRichSelect
}
