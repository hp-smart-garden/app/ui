/**
 * Service: Logger
 * @module logger
 * @description
 * Service: Logger
 * ```javascript
 * // Import
 * import Logger from '#services/logger'
 * ```
 */

// Classes
import Logger from './Logger'

/**
 * Logger
 * @alias module:logger
 * @type {Logger}
 * @constant
 * @description Logger service
 * See also [Logger class]{@link Logger}
 * @vuepress_syntax_block Logger
 * @vuepress_syntax_desc Access to Logger instance
 * @vuepress_syntax_ctx {root}
 * this.$Logger
 * @vuepress_syntax_ctx {component}
 * this.$Logger
 * this.$root.$Logger
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.$Logger
 */
export default new Logger()
