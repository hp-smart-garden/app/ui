// Classes
import ServiceAsPlugin from '#services/ServiceAsPlugin.js'
import Logger from '#services/logger'
import API from './API'

/**
 * APIManager class
 */
class APIManager extends ServiceAsPlugin {
  /**
   * Create a new API Manager
   * See also [API class]{@link API}
   * @param {API[]} APIs - An array of API
   */
  constructor (APIs) {
    const tmpAPIs = []
    // Loop on received options
    APIs.forEach((api) => {
      // Check if item is a instance of API
      if (api instanceof API) {
        tmpAPIs.push(api)
      } else {
        throw new Error('APIManager can\'t load API. One of them is not an API instance')
      }
    })
    const initVMData = {
      APIs: tmpAPIs
    }
    // Call ServiceAsPlugin constructor
    super(true, initVMData)
  }

  /**
   * APIs
   * @category properties
   * @readonly
   * @description Registered APIs
   * @return {API[]}
   */
  get APIs () {
    return this._vm.APIs
  }

  /**
   * add
   * @category methods
   * @description Add an API to manager anytime
   * @param {API} api - The API to add to manager
   * @return {API} Return this.use(<api.name>)
   */
  add (api) {
    // Check if item is a instance of API
    if (api instanceof API) {
      // Check in registered APIs
      const apiExists = this.APIs.find((a) => { return api.name === a.name })
      if (!apiExists) {
        this._vm.APIs.push(api)
      } else {
        Logger.consoleLog('warning', 'APIManager can\'t add API: "' + api.name + '" is already set...')
      }
    } else {
      throw new Error('APIManager can\'t load API. One of them is not an API instance')
    }
    return this.use(api.name)
  }

  /**
   * use
   * @category methods
   * @description Use a specific API that was loaded by the manager
   * @param {string} apiName - The API name to target
   * @return {API} Return the API
   */
  use (apiName) {
    // Check arg type
    if (typeof apiName !== 'string') {
      throw new TypeError('APIManager service : "use" method attempts String type for "apiName" parameter. Received : ' + typeof apiName)
    }
    // Find by name
    const api = this.APIs.find(e => e.name === apiName)
    // If found
    if (api) {
      return api
    } else {
      // Not found -> throw Error
      throw new Error('APIManager service : "use" method can\'t find API by name "' + apiName + '"')
    }
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue

    const pluginName = 'APIManager'

    // Create a 'reactive' mixin
    Vue.mixin(ServiceAsPlugin.createMixin(true, pluginName))

    // Define the getter on the 'private' property
    Object.defineProperty(Vue.prototype, '$' + pluginName, {
      get () { return this['_' + pluginName] }
    })
  }
}

export default APIManager
