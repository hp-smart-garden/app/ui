// Config
import CONFIG from '@/config/config.js'
// I18n
import I18n from '@/i18n/'
// utils
import { isDef } from '#utils'

/**
 * parseErrorResponse
 * @description Parse an error response from XHR request and return formatted object
 * @param {NetworkError} errorResponse - The error response
 * @return {Object}
 */
const parseErrorResponse = function (errorResponse) {
  let response = {}
  const status = errorResponse.response.status
  const data = errorResponse.response.data
  // Check if parsable
  const parsableResponse = data[CONFIG.API.ServerAPI.errorResponseKey]
  // If not parsable
  if (!isDef(parsableResponse)) {
    response.errorCode = 'e0010'
    // Define error code by response status
    if (status === 400) response.errorCode = 'e0010-400' // Bad Request
    if (status === 401) response.errorCode = 'e0010-401' // Unauthorized
    if (status === 402) response.errorCode = 'e0010-402' // Payment Required
    if (status === 403) response.errorCode = 'e0010-403' // Forbidden
    if (status === 404) response.errorCode = 'e0010-404' // Not found
    if (status === 405) response.errorCode = 'e0010-405' // Method Not Allowed
    if (status === 422) response.errorCode = 'e0010-422' // Unprocessable entity
    if (status === 500) response.errorCode = 'e0010-500' // Internal Server Error
    if (status === 501) response.errorCode = 'e0010-501' // Not Implemented
    if (status === 502) response.errorCode = 'e0010-502' // Bad Gateway / Proxy Error
    if (status === 503) response.errorCode = 'e0010-503' // Service Unavailable
    if (status === 504) response.errorCode = 'e0010-504' // Gateway Time-out
    response.errorMessage = I18n.t(`errors.${response.errorCode}`)
    response.data = data
    response.status = data
    response.payload = {}
  // Else: parsable
  } else {
    response = {
      errorCode: isDef(parsableResponse.errorCode) ? parsableResponse.errorCode : 'e0010',
      errorMessage: isDef(parsableResponse.errorMessage) ? parsableResponse.errorMessage : I18n.t('errors.e0010'),
      data: data,
      status: status,
      payload: isDef(parsableResponse.payload) ? parsableResponse.payload : {}
    }
  }
  return response
}

export default parseErrorResponse
