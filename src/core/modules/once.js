/**
 * @vuepress
 * ---
 * title: once
 * headline: "Core module: once"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Core module: once
 * @module once
 * @description
 * Core module: once
 * ```javascript
 * // Import
 * import once from '#modules/once'
 * ```
 */

/**
 * once
 * @alias module:once
 * @description once method: shortcut for EventBus $once method
 * @return {void}
 * @vuepress_syntax_block once
 * @vuepress_syntax_desc Access to once method, where `<args>` are EventBus.$once parameters
 * @vuepress_syntax_ctx {root}
 * this.once(...<args>)
 * @vuepress_syntax_ctx {component}
 * this.$root.once(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.once(...<args>)
 */
export default function once (...args) {
  return this.$EventBus.$once(...args)
}
