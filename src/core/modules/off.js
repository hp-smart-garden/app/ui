/**
 * @vuepress
 * ---
 * title: off
 * headline: "Core module: off"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Core module: off
 * @module off
 * @description
 * Core module: off
 * ```javascript
 * // Import
 * import off from '#modules/off'
 * ```
 */

/**
 * off
 * @alias module:off
 * @description off method: shortcut for EventBus $off method
 * @return {void}
 * @vuepress_syntax_block off
 * @vuepress_syntax_desc Access to off method, where `<args>` are EventBus.$off parameters
 * @vuepress_syntax_ctx {root}
 * this.off(...<args>)
 * @vuepress_syntax_ctx {component}
 * this.$root.off(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.off(...<args>)
 */
export default function off (...args) {
  return this.$EventBus.$off(...args)
}
