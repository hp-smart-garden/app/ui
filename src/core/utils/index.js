const debounce = require('./debounce')
const getHostHttpUrl = require('./getHostHttpUrl')
const isDef = require('./isDef')
const isEqual = require('./isEqual')
const isNull = require('./isNull')
const isValidHttpUrl = require('./isValidHttpUrl')
const maxBy = require('./maxBy')
const minBy = require('./minBy')
const random = require('./random')
const validateEmail = require('./validateEmail')
const validateHexColor = require('./validateHexColor')

module.exports = {
  debounce,
  getHostHttpUrl,
  isDef,
  isEqual,
  isNull,
  isValidHttpUrl,
  maxBy,
  minBy,
  random,
  validateEmail,
  validateHexColor
}
