// Lodash.minby
const lodashMinBy = require('lodash.minby')

/**
 * minBy
 * @description Find min by in array (lodash.minby).
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * minBy(array, function (item) { return item.id }) // Return -> { id: 1, name: 'John' }
 * minBy(array, function (item) { return item.name }) // Return -> { id: 2, name: 'Jane' }
 * @param {Array} array - The array
 * @param {Function} sortFct - The sort function
 * @return {string}
 */
const minBy = function (array, sortFct) {
  return lodashMinBy(array, sortFct)
}

module.exports = minBy
