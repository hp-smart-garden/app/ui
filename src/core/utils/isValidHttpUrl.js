/**
 * isValidHttpUrl
 * @description Return new URL object if str is a valid http url
 * @example
 * isValidHttpUrl('http://example.com/') // success: return new URL('http://example.com/')
 * isValidHttpUrl('example.com') // fail
 * @param {string} str - The url string
 * @return {(URL|boolean)}
 */
const isValidHttpUrl = function (str) {
  let url
  try {
    url = new URL(str)
  } catch (error) {
    return false
  }
  if (url.protocol === 'http:' || url.protocol === 'https:') {
    return url
  } else {
    return false
  }
}

module.exports = isValidHttpUrl
