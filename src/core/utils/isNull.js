/**
 * isNull
 * @description Return true if variable is null
 * @example
 * isNull(value) // => true is value is null
 * @param {*} value - The variable to check
 * @return {boolean}
 */
const isNull = function (value) {
  return value === null
}

module.exports = isNull
