// Lodash.maxby
const lodashMaxBy = require('lodash.maxby')

/**
 * maxBy
 * @description Find max by in array (lodash.maxby).
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * maxBy(array, function (item) { return item.id }) // Return -> { id: 2, name: 'Jane' }
 * maxBy(array, function (item) { return item.name }) // Return -> { id: 1, name: 'John' }
 * @param {Array} array - The array
 * @param {Function} sortFct - The sort function
 * @return {string}
 */
const maxBy = function (array, sortFct) {
  return lodashMaxBy(array, sortFct)
}

module.exports = maxBy
