/**
 * isDef
 * @description Return true if variable is defined (!== undefined)
 * @example
 * isDef(value) // => true is value is defined
 * @param {*} value - The variable to check
 * @return {boolean}
 */
const isDef = function (value) {
  return value !== undefined
}

module.exports = isDef
