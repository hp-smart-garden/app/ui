/**
 * Store module for users
 */

// Models
// import User from '#models/User'
// Logger
import Logger from '#services/logger'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Users
 * @alias module:store/modules/Users
 * @type {Object}
 */
const Users = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Users
   */
  state: {
    /**
     * createUserLoading
     * @description Indicates if request for create user is loading from API
     * @type {boolean}
     * @default false
     */
    createUserLoading: false,
    /**
     * updateUserLoading
     * @description Indicates if request for update user is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserLoading: false,
    /**
     * updateUserPasswordLoading
     * @description Indicates if request for update user password is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserPasswordLoading: false,
    /**
     * forgotPasswordLoading
     * @description Indicates if request for forgot password is loading from API
     * @type {boolean}
     * @default false
     */
    forgotPasswordLoading: false,
    /**
     * resetPasswordLoading
     * @description Indicates if request for reset password is loading from API
     * @type {boolean}
     * @default false
     */
    resetPasswordLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Users/<name>']
   */
  getters: {
    /**
     * getUser
     * @description Retrieve a user by id
     * @type {function}
     * @param {number} id - User id
     * @return {(User|undefined)}
     */
    getUser: (state) => (id) => {
      // Find user in state.users
      return state.users.find(user => user.id === id)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * createUser
     * @description Create user method.
     * Request `[POST] /api/users/create`
     * to create a new user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.data - Request data
     * @param {string} payload.data.username - User username
     * @param {string} payload.data.password - User password
     * @param {string} payload.data.password_confirmation - User password
     * @return {(boolean|Error)} Error or return true
     */
    createUser: async function ({ commit, dispatch }, { data }) {
      // Set createUserLoading to true
      commit('SET_CREATE_USER_LOADING', true)
      try {
        Logger.consoleLog('system', `Create user request - Request [POST] "${ServerAPI.options.baseURL}/users/create" ...`)
        await ServerAPI.request('POST', '/users/create', { data })
      } catch (error) {
        // Set createUserLoading to false
        commit('SET_CREATE_USER_LOADING', false)
        Logger.consoleLog('error', `Create user request error - Server response [POST] "${ServerAPI.options.baseURL}/users/create" ...`)
        // Return error
        return error
      }
      Logger.consoleLog('system', 'User created ✔')
      // Set createUserLoading to false
      commit('SET_CREATE_USER_LOADING', false)
      return true
    },
    /**
     * updateUser
     * @description Update user method.
     * Request `[PATCH] /api/users/<userId>`
     * to update an user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.userId - User id
     * @param {Object} payload.data - Request data -> user data (except: [password], see updateUserPassword)
     * @param {string} payload.data.username - User username
     * @param {string} payload.data.password - User password (for confirmation)
     * @return {(boolean|Error)} Error or return true
     */
    updateUser: async function ({ commit }, { userId, data }) {
      // Set updateUserLoading to true
      commit('SET_UPDATE_USER_LOADING', true)
      try {
        Logger.consoleLog('system', `Update user request - Request [PATCH] "${ServerAPI.options.baseURL}/users/${userId}" ...`)
        await ServerAPI.request('PATCH', `/users/${userId}`, { data })
      } catch (error) {
        // Set updateUserLoading to false
        commit('SET_UPDATE_USER_LOADING', false)
        Logger.consoleLog('error', `Update user request error - Server response [PATCH] "${ServerAPI.options.baseURL}/users/${userId}" ...`)
        return error
      }
      // Set updateUserLoading to false
      commit('SET_UPDATE_USER_LOADING', false)
      return true
    },
    /**
     * updateUserPassword
     * @description Update user password method.
     * Request `[PATCH] /api/me/password`
     * to update the password of an user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.data - Data
     * @param {string} payload.data.current_password - Current password
     * @param {string} payload.data.password - New password
     * @param {string} payload.data.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    updateUserPassword: async function ({ commit }, { data }) {
      // Set updateUserPasswordLoading to true
      commit('SET_UPDATE_USER_PASSWORD_LOADING', true)
      try {
        Logger.consoleLog('system', `Update user password request - Request [PATCH] "${ServerAPI.options.baseURL}/me/password" ...`)
        await ServerAPI.request('PATCH', '/me/password', { data })
      } catch (error) {
        // Set updateUserPasswordLoading to false
        commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
        Logger.consoleLog('error', `Update user password request error - Server response [PATCH] "${ServerAPI.options.baseURL}/me/password" ...`)
        return error
      }
      // Set updateUserPasswordLoading to false
      commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
      return true
    },
    /**
     * forgotPassword
     * @description Send reset password request with email.
     * Request `[POST] /api/forgot-password`
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User email
     * @return {(boolean|Error)} Error or return true
     */
    forgotPassword: async function ({ commit, dispatch }, payload) {
      // Set forgotPasswordLoading to true
      commit('SET_FORGOT_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/forgot-password', {
          data: payload
        })
      } catch (error) {
        // Set forgotPasswordLoading to false
        commit('SET_FORGOT_PASSWORD_LOADING', false)
        return error
      }
      // Set forgotPasswordLoading to false
      commit('SET_FORGOT_PASSWORD_LOADING', false)
      return true
    },
    /**
     * resetPassword
     * @description Reset user password method.
     * Request `[POST] /api/reset-password`
     * to reset a user password.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User email
     * @param {string} payload.password - New password
     * @param {string} payload.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    resetPassword: async function ({ commit, dispatch }, payload) {
      // Set resetPasswordLoading to true
      commit('SET_RESET_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/reset-password', {
          data: payload
        })
      } catch (error) {
        // Set resetPasswordLoading to false
        commit('SET_RESET_PASSWORD_LOADING', false)
        return error
      }
      // Set resetPasswordLoading to false
      commit('SET_RESET_PASSWORD_LOADING', false)
      return true
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Users/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_CREATE_USER_LOADING
     * @description Mutate state.createUserLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CREATE_USER_LOADING (state, value) {
      state.createUserLoading = value
    },
    /**
     * SET_UPDATE_USER_LOADING
     * @description Mutate state.updateUserLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_LOADING (state, value) {
      state.updateUserLoading = value
    },
    /**
     * SET_UPDATE_USER_PASSWORD_LOADING
     * @description Mutate state.updateUserPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_PASSWORD_LOADING (state, value) {
      state.updateUserPasswordLoading = value
    },
    /**
     * SET_FORGOT_PASSWORD_LOADING
     * @description Mutate state.forgotPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_FORGOT_PASSWORD_LOADING (state, value) {
      state.forgotPasswordLoading = value
    },
    /**
     * SET_RESET_PASSWORD_LOADING
     * @description Mutate state.resetPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_RESET_PASSWORD_LOADING (state, value) {
      state.resetPasswordLoading = value
    }
  }
}

export default Users
