/**
 * Store module for authentication
 */

// Configuration
import CONFIG from '@/config/config.js'
// Models
import User from '#models/User'
// Logger
import Logger from '#services/logger'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Auth
 * @alias module:store/modules/Auth
 * @type {Object}
 */
const Auth = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Auth
   */
  state: {
    /**
     * currentUser
     * @description Current / Authenticated User
     * @type {?User}
     * @default null
     */
    currentUser: null,
    /**
     * currentUserLoading
     * @description Indicates if current user is loading from API
     * @type {boolean}
     * @default false
     */
    currentUserLoading: false,
    /**
     * passwordForSecurity
     * @description When user enter password for securized features
     * @type {string}
     * @default '' (empty string)
     */
    passwordForSecurity: ''
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Auth/<name>']
   */
  getters: {
    // Nothing here...
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * login
     * @description Request `[POST] /api/login` with credentials data.
     * If server side success, save tokens in LocalStorage and dispatch 'checkAuthenticated'
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.data - Request data
     * @param {string} payload.data.username - Username
     * @param {string} payload.data.password - Password
     * @return {(boolean|Error)} Return value of 'checkAuthenticated'
     */
    login: async function ({ commit, dispatch }, { data }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      // Request for auth
      try {
        Logger.consoleLog('system', `Login request - Request [POST] "${ServerAPI.options.baseURL}/login" ...`)
        const authResponse = await ServerAPI.request('POST', '/login', {
          data: data
        })
        const accessToken = authResponse.data.access_token
        // refreshToken = authResponse.data.refresh_token
        Logger.consoleLog('system', 'Set access token in localStorage')
        // Save tokens in localStorage
        localStorage.setItem(CONFIG.AUTH.ACCESS_TOKEN_NAME, accessToken)
        // localStorage.setItem(CONFIG.AUTH.REFRESH_TOKEN_NAME, refreshToken)
      } catch (error) {
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        Logger.consoleLog('error', `Login request error - Server response [POST] "${ServerAPI.options.baseURL}/login" ...`)
        return error
      }
      return true
    },
    /**
     * logout
     * @description Remove tokens from the local storage and reset current user
     * @param {Object} context - vuex context
     * @return {void}
     */
    logout: async function ({ commit }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      try {
        // Request for logout
        Logger.consoleLog('system', `Logout request - Request [POST] "${ServerAPI.options.baseURL}/logout" ...`)
        await ServerAPI.request('POST', '/logout')
      } catch (error) {
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        Logger.consoleLog('error', `Logout request error - Server response [POST] "${ServerAPI.options.baseURL}/logout" ...`)
        return error
      }
      // Remove access token
      Logger.consoleLog('system', 'Remove tokens from localStorage')
      localStorage.removeItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      localStorage.removeItem(CONFIG.AUTH.REFRESH_TOKEN_NAME)
      // Set currentUserLoading to false
      commit('SET_CURRENT_USER_LOADING', false)
      // Reset currentUser
      commit('RESET_CURRENT_USER')
    },
    /**
     * checkAuthenticated
     * @description Check if user is authenticated.
     * First, check if token is set in the local storage,
     * set authorization header and request for user data from server.
     * Else, call 'logout' and return false.
     * @param {Object} context - vuex context
     * @return {(boolean|Error)} False or return value of 'loadCurrentUserData'
     */
    checkAuthenticated: async function ({ commit, dispatch }) {
      Logger.consoleLog('system', 'Check access token in localStorage...')
      // Retrieve token in localStorage
      const accessToken = localStorage.getItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      // const refreshToken = localStorage.getItem('ms_refresh_token')
      // If no access token
      if (!accessToken) {
        Logger.consoleLog('system', 'Not authenticated ❌')
        return false
      }
      Logger.consoleLog('system', 'Access token found ✔')
      // Set token to headers for each future requests
      dispatch('setAuthorizationHeader', { token: accessToken })
      // Get User data
      return dispatch('loadCurrentUserData')
    },
    /**
     * loadCurrentUserData
     * @description Load current user data from API server.
     * Request `[GET] /api/me` (See [API Server documentation](/guide/system/api-server/)).
     * @param {Object} context - vuex context
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadCurrentUserData: async function ({ commit, dispatch }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      // Get authenticated user data
      try {
        Logger.consoleLog('system', `Load current user data - Request [GET] "${ServerAPI.options.baseURL}/me" ...`)
        const userResponse = await ServerAPI.request('GET', '/me')
        // Set user in state
        commit('SET_CURRENT_USER', userResponse.data)
        Logger.consoleLog('system', 'Authenticated ✔')
      } catch (error) {
        // Authentication token error
        // Remove token and reset user
        dispatch('logout')
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        Logger.consoleLog('error', `Authentication token error - Server response [GET] "${ServerAPI.options.baseURL}/me" ...`)
        return error
      }
      // Set currentUserLoading to false
      commit('SET_CURRENT_USER_LOADING', false)
      // Return true
      return true
    },
    /**
     * setAuthorizationHeader
     * @description Set the authorization token header for the API instance (ServerAPI).
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.token - The access token
     * @return {void}
     */
    setAuthorizationHeader: function ({ commit }, { token }) {
      Logger.consoleLog('system', 'Set authorization headers (token) for future requests to Server API')
      // Set authorization token
      ServerAPI.driver.defaults.headers.common.Authorization = 'Bearer ' + token
    },
    /**
     * resetAuthorizationHeader
     * @description Delete the authorization token header for the API instance (ServerAPI).
     * @param {Object} context - vuex context
     * @return {void}
     */
    resetAuthorizationHeader ({ commit }) {
      Logger.consoleLog('system', 'Reset authorization headers of Server API')
      // Delete header
      delete ServerAPI.driver.defaults.headers.common.Authorization
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Auth/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_CURRENT_USER
     * @description Mutate state.currentUser
     * @param {Object} state - vuex store state
     * @param {Object} user - Data received from server
     * @return {void}
     */
    SET_CURRENT_USER (state, user) {
      state.currentUser = new User(user)
      state.currentUser.authenticated = true
    },
    /**
     * RESET_CURRENT_USER
     * @description Mutate state.currentUser
     * @param {Object} state - vuex store state
     * @return {void}
     */
    RESET_CURRENT_USER (state) {
      state.currentUser = null
    },
    /**
     * SET_CURRENT_USER_LOADING
     * @description Mutate state.currentUserLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CURRENT_USER_LOADING (state, value) {
      state.currentUserLoading = value
    },
    /**
     * SET_PASSWORD_FOR_SECURITY
     * @description Mutate state.passwordForSecurity
     * @param {Object} state - vuex store state
     * @param {string} value - Password
     * @return {void}
     */
    SET_PASSWORD_FOR_SECURITY (state, value) {
      state.passwordForSecurity = value
    },
    /**
     * RESET_PASSWORD_FOR_SECURITY
     * @description Mutate state.passwordForSecurity
     * @param {Object} state - vuex store state
     * @return {void}
     */
    RESET_PASSWORD_FOR_SECURITY (state) {
      state.passwordForSecurity = ''
    }
  }
}

export default Auth
