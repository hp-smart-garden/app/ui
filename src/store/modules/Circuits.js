/**
 * Store module for circuits
 */

// Models
import Circuit from '#models/Circuit'
// Logger
import Logger from '#services/logger'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Circuits
 * @alias module:store/modules/Circuits
 * @type {Object}
 */
const Circuits = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Circuits
   */
  state: {
    /**
     * circuits
     * @description Contains loaded circuits
     * @type {Circuit[]}
     * @default []
     */
    circuits: [],
    /**
     * circuitsLoading
     * @description Indicates if circuits are loading from API
     * @type {boolean}
     * @default false
     */
    circuitsLoading: false,
    /**
     * createCircuitLoading
     * @description Indicates if request for create circuit is loading from API
     * @type {boolean}
     * @default false
     */
    createCircuitLoading: false,
    /**
     * updateCircuitLoading
     * @description Indicates if request for update circuit is loading from API
     * @type {boolean}
     * @default false
     */
    updateCircuitLoading: false,
    /**
     * deleteCircuitLoading
     * @description Indicates if request for delete circuit is loading from API
     * @type {boolean}
     * @default false
     */
    deleteCircuitLoading: false,
    /**
     * circuitStatusLoading
     * @description Indicates if request for circuit status is loading from API
     * @type {boolean}
     * @default false
     */
    circuitStatusLoading: false,
    /**
     * turnCircuitLoading
     * @description Indicates if request for turn circuit is loading from API
     * @type {boolean}
     * @default false
     */
    turnCircuitLoading: false,
    /**
     * circuitToEdit
     * @description Circuit ID to edit
     * @type {?number}
     * @default null
     */
    circuitToEdit: null,
    /**
     * circuitToDelete
     * @description Circuit ID to delete
     * @type {?number}
     * @default null
     */
    circuitToDelete: null
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Circuits/<name>']
   */
  getters: {
    /**
     * getCircuit
     * @description Retrieve a circuit by id
     * @type {function}
     * @param {number} id - Circuit id
     * @return {(Circuit|undefined)}
     */
    getCircuit: (state) => (id) => {
      // Find circuit in state.circuits
      return state.circuits.find(circuit => circuit.id === id)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Circuits/<name>')
   */
  actions: {
    /**
     * loadCircuits
     * @description Load circuits data from API server.
     * Request `[GET] /api/circuits`
     * to get circuits data
     * @param {Object} context - vuex context
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadCircuits: async function ({ commit, dispatch }) {
      // Set circuitsLoading to true
      commit('SET_CIRCUITS_LOADING', true)
      // Get circuits data
      try {
        Logger.consoleLog('system', `Get circuits request - Request [GET] "${ServerAPI.options.baseURL}/circuits" ...`)
        const response = await ServerAPI.request('GET', '/circuits')
        // Set circuits in state
        commit('SET_CIRCUITS', response.data)
      } catch (error) {
        commit('SET_CIRCUITS_LOADING', false)
        Logger.consoleLog('error', `Get circuits request error - Server response [GET] "${ServerAPI.options.baseURL}/circuits" ...`)
        return error
      }
      // Set circuitsLoading to false
      commit('SET_CIRCUITS_LOADING', false)
      // Return true
      return true
    },
    /**
     * createCircuit
     * @description Create circuit method.
     * Request `[POST] /api/circuits/create`
     * to create a new circuit.
     * @param {Object}  context - vuex context
     * @param {Object}  data - Request data
     * @param {number}  data.channel - Circuit channel
     * @param {string}  data.name - Circuit name
     * @param {string}  data.color - Circuit color (hex format: '#XXXXXX')
     * @param {?string} [data.description] - Circuit description
     * @return {(boolean|Error)} Error or return true
     */
    createCircuit: async function ({ commit, dispatch }, data) {
      // Set createCircuitLoading to true
      commit('SET_CREATE_CIRCUIT_LOADING', true)
      try {
        Logger.consoleLog('system', `Create circuit request - Request [POST] "${ServerAPI.options.baseURL}/circuits/create" ...`)
        await ServerAPI.request('POST', '/circuits/create', { data })
      } catch (error) {
        // Set createCircuitLoading to false
        commit('SET_CREATE_CIRCUIT_LOADING', false)
        Logger.consoleLog('error', `Create circuit request error - Server response [POST] "${ServerAPI.options.baseURL}/circuits/create" ...`)
        // Return error
        return error
      }
      Logger.consoleLog('system', 'Circuit created ✔')
      // Set createCircuitLoading to false
      commit('SET_CREATE_CIRCUIT_LOADING', false)
      return true
    },
    /**
     * updateCircuit
     * @description Update circuit method.
     * Request `[PATCH] /api/circuits/<circuitId>`
     * to update a circuit.
     * @param {Object}  context - vuex context
     * @param {Object}  payload - Method payload
     * @param {string}  payload.circuitId - Circuit id
     * @param {Object}  payload.data - Request data
     * @param {number}  [payload.data.channel] - Circuit channel
     * @param {string}  [payload.data.name] - Circuit name
     * @param {string}  [payload.data.color] - Circuit color (hex format: '#XXXXXX')
     * @param {?string} [payload.data.description] - Circuit description
     * @return {(boolean|Error)} Error or return true
     */
    updateCircuit: async function ({ commit }, { circuitId, data }) {
      // Set updateCircuitLoading to true
      commit('SET_UPDATE_CIRCUIT_LOADING', true)
      try {
        Logger.consoleLog('system', `Update circuit request - Request [PATCH] "${ServerAPI.options.baseURL}/circuits/${circuitId}" ...`)
        await ServerAPI.request('PATCH', `/circuits/${circuitId}`, { data })
      } catch (error) {
        // Set updateCircuitLoading to false
        commit('SET_UPDATE_CIRCUIT_LOADING', false)
        Logger.consoleLog('error', `Update circuit request error - Server response [PATCH] "${ServerAPI.options.baseURL}/circuits/${circuitId}" ...`)
        return error
      }
      // Set updateCircuitLoading to false
      commit('SET_UPDATE_CIRCUIT_LOADING', false)
      return true
    },
    /**
     * deleteCircuit
     * @description Delete circuit method.
     * Request `[DELETE] /api/circuits/<circuitId>`
     * to delete a circuit.
     * @param {Object} context - vuex context
     * @param {number} circuitId - Circuit id
     * @return {(boolean|Error)} Error or return true
     */
    deleteCircuit: async function ({ commit }, circuitId) {
      // Set deleteCircuitLoading to true
      commit('SET_DELETE_CIRCUIT_LOADING', true)
      try {
        Logger.consoleLog('system', `Delete circuit request - Request [DELETE] "${ServerAPI.options.baseURL}/circuits/${circuitId}" ...`)
        await ServerAPI.request('DELETE', `/circuits/${circuitId}`)
      } catch (error) {
        // Set deleteCircuitLoading to false
        commit('SET_DELETE_CIRCUIT_LOADING', false)
        Logger.consoleLog('error', `Delete circuit request error - Server response [DELETE] "${ServerAPI.options.baseURL}/circuits/${circuitId}" ...`)
        return error
      }
      // Set deleteCircuitLoading to false
      commit('SET_DELETE_CIRCUIT_LOADING', false)
      return true
    },
    /**
     * getCircuitStatus
     * @description Get circuit status method.
     * Request `[GET] /api/circuits/<circuitId>/status`
     * to get a circuit status.
     * @param {Object} context - vuex context
     * @param {number} circuitId - Circuit id
     * @return {(boolean|Error)} Error or return true
     */
    getCircuitStatus: async function ({ commit, getters }, circuitId) {
      // Set circuitStatusLoading to true
      commit('SET_CIRCUIT_STATUS_LOADING', true)
      try {
        Logger.consoleLog('system', `Get circuit status request - Request [GET] "${ServerAPI.options.baseURL}/circuits/${circuitId}/status" ...`)
        const response = await ServerAPI.request('GET', `/circuits/${circuitId}/status`)
        // Set status
        getters.getCircuit(circuitId).setStatus(response.data.status)
      } catch (error) {
        // Set circuitStatusLoading to false
        commit('SET_CIRCUIT_STATUS_LOADING', false)
        Logger.consoleLog('error', `Get circuit status request error - Server response [GET] "${ServerAPI.options.baseURL}/circuits/${circuitId}/status" ...`)
        return error
      }
      // Set circuitStatusLoading to false
      commit('SET_CIRCUIT_STATUS_LOADING', false)
      return true
    },
    /**
     * turnCircuit
     * @description Turn circuit on/off method.
     * Request `[POST] /api/circuits/<circuitId>/turn`
     * to turn a circuit on/off.
     * @param {Object} context - vuex context
     * @param {Object} payload - vuex context
     * @param {number} payload.circuitId - Circuit id
     * @param {boolean} payload.status - Circuit status
     * @return {(boolean|Error)} Error or return true
     */
    turnCircuit: async function ({ commit, getters }, { circuitId, status }) {
      // Set turnCircuitLoading to true
      commit('SET_TURN_CIRCUIT_LOADING', true)
      try {
        Logger.consoleLog('system', `Turn circuit request - Request [POST] "${ServerAPI.options.baseURL}/circuits/${circuitId}/turn" ...`)
        const response = await ServerAPI.request('POST', `/circuits/${circuitId}/turn`, {
          data: {
            status: status
          }
        })
        // Set status
        getters.getCircuit(circuitId).setStatus(response.data.status)
      } catch (error) {
        // Set turnCircuitLoading to false
        commit('SET_TURN_CIRCUIT_LOADING', false)
        Logger.consoleLog('error', `Turn circuit request error - Server response [POST] "${ServerAPI.options.baseURL}/circuits/${circuitId}/turn" ...`)
        return error
      }
      // Set turnCircuitLoading to false
      commit('SET_TURN_CIRCUIT_LOADING', false)
      return true
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Circuits/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_CIRCUITS
     * @description Mutate state.circuits
     * @param {Object} state - vuex store state
     * @param {Object[]} circuits - Data received from server
     * @return {void}
     */
    SET_CIRCUITS (state, circuits) {
      state.circuits = circuits.map((circuit) => {
        return new Circuit(circuit)
      })
    },
    /**
     * SET_CIRCUITS_LOADING
     * @description Mutate state.circuitsLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CIRCUITS_LOADING (state, value) {
      state.circuitsLoading = value
    },
    /**
     * SET_CREATE_CIRCUIT_LOADING
     * @description Mutate state.createCircuitLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CREATE_CIRCUIT_LOADING (state, value) {
      state.createCircuitLoading = value
    },
    /**
     * SET_UPDATE_CIRCUIT_LOADING
     * @description Mutate state.updateCircuitLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_CIRCUIT_LOADING (state, value) {
      state.updateCircuitLoading = value
    },
    /**
     * SET_DELETE_CIRCUIT_LOADING
     * @description Mutate state.deleteCircuitLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_DELETE_CIRCUIT_LOADING (state, value) {
      state.deleteCircuitLoading = value
    },
    /**
     * SET_CIRCUIT_STATUS_LOADING
     * @description Mutate state.circuitStatusLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CIRCUIT_STATUS_LOADING (state, value) {
      state.circuitStatusLoading = value
    },
    /**
     * SET_TURN_CIRCUIT_LOADING
     * @description Mutate state.turnCircuitLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_TURN_CIRCUIT_LOADING (state, value) {
      state.turnCircuitLoading = value
    },
    /**
     * SET_CIRCUIT_TO_EDIT
     * @description Mutate state.circuitToEdit
     * @param {Object} state - vuex store state
     * @param {?number} circuitId - Circuit ID (or null)
     * @return {void}
     */
    SET_CIRCUIT_TO_EDIT (state, circuitId) {
      state.circuitToEdit = circuitId
    },
    /**
     * SET_CIRCUIT_TO_DELETE
     * @description Mutate state.circuitToDelete
     * @param {Object} state - vuex store state
     * @param {?number} circuitId - Circuit ID (or null)
     * @return {void}
     */
    SET_CIRCUIT_TO_DELETE (state, circuitId) {
      state.circuitToDelete = circuitId
    }
  }
}

export default Circuits
