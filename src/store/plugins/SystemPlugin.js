// I18n
import I18n from '@/i18n/'
/**
 * vuex store System Plugin
 */
export default (store) => {
  store.subscribe((mutation, state) => {
    // Main loading observer
    const toMirror = [
      // For Auth user loading observer in state.Auth
      { mutation: 'Auth/SET_CURRENT_USER_LOADING', message: I18n.t('user.currentUserLoading') },
      // For each 'Users' resource loading observer state.Users
      { mutation: 'Users/SET_CREATE_USER_LOADING', message: I18n.t('user.createUserLoading') },
      { mutation: 'Users/SET_UPDATE_USER_LOADING', message: I18n.t('user.updateUserLoading') },
      { mutation: 'Users/SET_UPDATE_USER_PASSWORD_LOADING', message: I18n.t('user.updateUserPasswordLoading') },
      { mutation: 'Users/SET_FORGOT_PASSWORD_LOADING', message: I18n.t('global.loading') },
      { mutation: 'Users/SET_RESET_PASSWORD_LOADING', message: I18n.t('global.loading') },
      // For each 'Circuits' resource loading observer state.Circuits
      { mutation: 'Circuits/SET_CIRCUITS_LOADING', message: I18n.t('circuit.circuitsLoading') },
      { mutation: 'Circuits/SET_CREATE_CIRCUIT_LOADING', message: I18n.t('circuit.createCircuitLoading') },
      { mutation: 'Circuits/SET_UPDATE_CIRCUIT_LOADING', message: I18n.t('circuit.updateCircuitLoading') },
      { mutation: 'Circuits/SET_DELETE_CIRCUIT_LOADING', message: I18n.t('circuit.deleteCircuitLoading') },
      { mutation: 'Circuits/SET_CIRCUIT_STATUS_LOADING', message: I18n.t('circuit.circuitStatusLoading') },
      { mutation: 'Circuits/SET_TURN_CIRCUIT_LOADING', message: I18n.t('global.loading') }
    ]
    // If match mutation -> display main loading
    const match = toMirror.find(m => m.mutation === mutation.type)
    if (match) {
      store.commit('SET_SYSTEM_MAIN_LOADING_MESSAGE', match.message)
      store.commit('SET_SYSTEM_SHOW_MAIN_LOADING', mutation.payload)
    }
    // appelé après chaque mutation.
    // Les mutations arrivent au format `{ type, payload }`.
  })
}
