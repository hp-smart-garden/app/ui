# Errors register (for translations)

Please check the [server documentation](https://gitlab.com/hperchec-boilerplates/scorpion/server) to understand how errors are managed.

> ❗ Don't edit *.json files in this directory
> Follow server instructions:
> - update and export from server
> - then copy/paste generated files in this directory
