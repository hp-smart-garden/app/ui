// utils
import { isDef } from '#utils'

const DEFAULTS = (prop) => Circuit.defaults()[prop]

/**
 * Circuit class
 */
class Circuit {
  /**
   * @private
   */
  _id;
  _channel;
  _name;
  _color;
  _type;
  _description;
  _status;
  _history;

  /**
   * Create a Circuit
   * @param {Object} data - Object that represents the circuit
   * @param {number} data.id - The unique circuit id
   * @param {number} data.channel - The circuit channel
   * @param {string} data.name - The circuit name
   * @param {string} data.color - The circuit color
   * @param {string} data.type - The circuit type
   * @param {Object[]} data.history - The circuit history
   * @param {?string} data.description - The circuit description
   */
  constructor (data) {
    // If data is undefined -> called as `new Circuit()`
    const __EMPTY__ = !isDef(data)
    data = __EMPTY__ ? DEFAULTS : data
    // Assign properties
    this._id = data.id
    this._channel = data.channel
    this._name = data.name
    this._color = data.color
    this._type = data.type
    this.setHistory(data.history)
    this._description = data.description
    // Status to off by default
    this._status = false
  }

  /**
   * Accessors & mutators
   */

  /**
   * id
   * @category properties
   * @readonly
   * @description Circuit id accessor
   * @type {number}
   */
  get id () {
    return this._id
  }

  /**
   * channel
   * @category properties
   * @readonly
   * @description Circuit channel accessor
   * @type {number}
   */
  get channel () {
    return this._channel
  }

  /**
   * name
   * @category properties
   * @readonly
   * @description Circuit name accessor
   * @type {string}
   */
  get name () {
    return this._name
  }

  /**
   * color
   * @category properties
   * @readonly
   * @description Circuit color accessor
   * @type {string}
   */
  get color () {
    return this._color
  }

  /**
   * type
   * @category properties
   * @readonly
   * @description Circuit type accessor
   * @type {string}
   */
  get type () {
    return this._type
  }

  /**
   * history
   * @category properties
   * @readonly
   * @description Circuit history accessor
   * @type {Object[]}
   */
  get history () {
    return this._history
  }

  /**
   * description
   * @category properties
   * @readonly
   * @description Circuit description accessor
   * @type {?string}
   */
  get description () {
    return this._description
  }

  /**
   * status
   * @category properties
   * @readonly
   * @description Circuit status accessor
   * @type {boolean}
   */
  get status () {
    return this._status
  }

  /**
   * Methods
   */

  /**
   * Set status
   * @param {boolean} value - True ('on') or false ('off')
   */
  setStatus (value) {
    if (typeof value !== 'boolean') {
      throw new Error('Circuit class - "status" setter error: value must be boolean')
    }
    this._status = value
  }

  /**
   * setHistory
   * @param {Object[]} data - History received from server
   */
  setHistory (data) {
    // First, cast some properties for each item
    data = data.map((item) => {
      item.created_at = new Date(item.created_at) // Cast 'created_at' to Date()
      return item
    })
    // Then, format data
    let isThereMissingData = false // False by default
    const formatted = data.reduce((array, item, index) => {
      // Check if item is status = 'on'
      if (item.status) {
        // Check if next index is status = 'off'
        const nextItem = data[index + 1]
        if (isDef(nextItem) && !nextItem.status) {
          array.push({
            start: item.created_at,
            end: nextItem.created_at
          })
        } else {
          isThereMissingData = true
          array.push({
            start: item.created_at,
            end: null
          })
        }
      }
      return array
    }, [])
    // Set history object
    this._history = {
      original: data,
      formatted: formatted,
      isThereMissingData: isThereMissingData
    }
  }

  /**
   * Defaults
   */

  /**
   * Default properties for a new empty 'Circuit' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      id: null,
      channel: null,
      name: null,
      color: null,
      type: 'valve',
      description: null,
      history: []
    }
  }
}

export default Circuit
