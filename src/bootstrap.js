/**
 * Packages
 */
// vue
import Vue from 'vue'
// iso-i18n
import languages from '@cospired/i18n-iso-languages'
// vue-ripple-directive
import Ripple from 'vue-ripple-directive'
// Font awesome icons
import 'vue-awesome/icons'
// utils
import { isDef } from '#utils'

/**
 * Services
 */
import Logger from '#services/logger'

/**
 * Services (plugins)
 */
// Reactive (ServiceAsPlugin inheritance)
import APIManagerService from '#services/api-manager/APIManager'
// Not reactive services
import HelpersService from '#services/helpers/Helpers'
import LoggerService from '#services/logger/Logger'
import EventBusService from '#services/event-bus/EventBus'

/**
 * Plugins
 */
// vuex
import Vuex from 'vuex'
// vue-router
import VueRouter from 'vue-router'
// vue-i18n
import VueI18n from 'vue-i18n'
// VueTailwind
import VueTailwind from 'vue-tailwind'
// Tailwind components & configuration
import { VTWSettings } from '@/components/tailwind/index.js'

/**
 * Mixins
 */
// Global mixin
import GlobalMixin from '#mixins/GlobalMixin'

/**
 * Components
 */
// Vue-awesome
import Icon from 'vue-awesome/components/Icon'

// Info for development mode
Logger.consoleLog('info', 'Development mode enabled', { prod: false })

/**
 * Vue config
 */
// Hide production message in the web browser console
// Vue.config.productionTip = process.env.NODE_ENV === 'development'
Vue.config.productionTip = false

/**
 * Plugins utilisation
 */

Logger.consoleLog('system', 'Plugins use...')

// Store
Vue.use(Vuex)
// Router
Vue.use(VueRouter)
// I18n
Vue.use(VueI18n)
// Core services
Vue.use(APIManagerService)
Vue.use(HelpersService)
Vue.use(LoggerService)
Vue.use(EventBusService)
// Others
Vue.use(VueTailwind, VTWSettings)

/**
 * Mixins utilisation
 */

Logger.consoleLog('system', 'Define mixins...')

Vue.mixin(GlobalMixin)

/**
 * Directives declaration
 */

Logger.consoleLog('system', 'Define directives...')

Ripple.color = 'rgba(255, 255, 255, 0.35)' // Default
Ripple.zIndex = 55 // Default
Vue.directive('ripple', Ripple)

/**
 * Components registration
 */

Logger.consoleLog('system', 'Register commons components...')

// Commons
const requireCommonComponents = require.context('./components/commons', true, /.*\.vue$/)
const commonComponents = {}
// Loop on common components
requireCommonComponents.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireCommonComponents(fileName)
  // Get name of component
  const componentName = componentConfig.default.name
  // Check if component has "name" property defined
  if (!isDef(componentName)) {
    Logger.consoleLog('error', `Error during "commons" components registration: component "${fileName}" doesn't have "name" property defined. Please give a name to the component.`)
    console.warn(`Error during "commons" components registration: component "${fileName}" doesn't have "name" property defined. Please give a name to the component.`)
  } else {
    // Check if component has a name already taken
    if (isDef(commonComponents[componentName])) {
      Logger.consoleLog('error', `Error during "commons" components registration: component name "${componentName}" already taken. Please provide an available name.`)
      console.warn(`Error during "commons" components registration: component name "${componentName}" already taken. Please provide an available name.`)
    } else {
      // All the component was exported with `export default`
      commonComponents[componentName] = componentConfig.default
    }
  }
})

for (const name in commonComponents) {
  // Register component globally
  Vue.component(name, commonComponents[name])
}

Logger.consoleLog('system', 'Register vendors components...')

// Vue-awesome
Vue.component('v-icon', Icon)

/**
 * Register french & english languages
 */

Logger.consoleLog('system', 'Register locale \'en\'...')
languages.registerLocale(require('@cospired/i18n-iso-languages/langs/en.json'))

Logger.consoleLog('system', 'Register locale \'fr\'...')
languages.registerLocale(require('@cospired/i18n-iso-languages/langs/fr.json'))
