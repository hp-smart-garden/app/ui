const Color = require('color')

const { THEME } = require('./globals.config.json')

/* eslint-disable key-spacing */

module.exports = {
  purge: [
    './public/index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    screens: {
      sm:     `${THEME.__THEME_BREAKPOINTS_SM__}px`,
      md:     `${THEME.__THEME_BREAKPOINTS_MD__}px`,
      lg:     `${THEME.__THEME_BREAKPOINTS_LG__}px`,
      xl:     `${THEME.__THEME_BREAKPOINTS_XL__}px`,
      '2xl':  `${THEME.__THEME_BREAKPOINTS_2XL__}px`
    },
    fill: theme => ({
      dark: theme('colors.dark')
    }),
    extend: {
      colors: {
        primary:            THEME.__THEME_PRIMARY_COLOR__,
        'primary-100':      Color(THEME.__THEME_PRIMARY_COLOR__).lighten(0.4).hex(),
        'primary-200':      Color(THEME.__THEME_PRIMARY_COLOR__).lighten(0.3).hex(),
        'primary-300':      Color(THEME.__THEME_PRIMARY_COLOR__).lighten(0.2).hex(),
        'primary-400':      Color(THEME.__THEME_PRIMARY_COLOR__).lighten(0.1).hex(),
        'primary-500':      THEME.__THEME_PRIMARY_COLOR__,
        'primary-600':      Color(THEME.__THEME_PRIMARY_COLOR__).darken(0.1).hex(),
        'primary-700':      Color(THEME.__THEME_PRIMARY_COLOR__).darken(0.2).hex(),
        'primary-800':      Color(THEME.__THEME_PRIMARY_COLOR__).darken(0.3).hex(),
        'primary-900':      Color(THEME.__THEME_PRIMARY_COLOR__).darken(0.4).hex(),
        secondary:          THEME.__THEME_SECONDARY_COLOR__,
        'secondary-100':    Color(THEME.__THEME_SECONDARY_COLOR__).lighten(0.4).hex(),
        'secondary-200':    Color(THEME.__THEME_SECONDARY_COLOR__).lighten(0.3).hex(),
        'secondary-300':    Color(THEME.__THEME_SECONDARY_COLOR__).lighten(0.2).hex(),
        'secondary-400':    Color(THEME.__THEME_SECONDARY_COLOR__).lighten(0.1).hex(),
        'secondary-500':    THEME.__THEME_SECONDARY_COLOR__,
        'secondary-600':    Color(THEME.__THEME_SECONDARY_COLOR__).darken(0.1).hex(),
        'secondary-700':    Color(THEME.__THEME_SECONDARY_COLOR__).darken(0.2).hex(),
        'secondary-800':    Color(THEME.__THEME_SECONDARY_COLOR__).darken(0.3).hex(),
        'secondary-900':    Color(THEME.__THEME_SECONDARY_COLOR__).darken(0.4).hex(),
        tertiary:           THEME.__THEME_TERTIARY_COLOR__,
        'tertiary-100':     Color(THEME.__THEME_TERTIARY_COLOR__).lighten(0.4).hex(),
        'tertiary-200':     Color(THEME.__THEME_TERTIARY_COLOR__).lighten(0.3).hex(),
        'tertiary-300':     Color(THEME.__THEME_TERTIARY_COLOR__).lighten(0.2).hex(),
        'tertiary-400':     Color(THEME.__THEME_TERTIARY_COLOR__).lighten(0.1).hex(),
        'tertiary-500':     THEME.__THEME_TERTIARY_COLOR__,
        'tertiary-600':     Color(THEME.__THEME_TERTIARY_COLOR__).darken(0.1).hex(),
        'tertiary-700':     Color(THEME.__THEME_TERTIARY_COLOR__).darken(0.2).hex(),
        'tertiary-800':     Color(THEME.__THEME_TERTIARY_COLOR__).darken(0.3).hex(),
        'tertiary-900':     Color(THEME.__THEME_TERTIARY_COLOR__).darken(0.4).hex(),
        success:            THEME.__THEME_SUCCESS_COLOR__,
        info:               THEME.__THEME_INFO_COLOR__,
        warning:            THEME.__THEME_WARNING_COLOR__,
        'warning-500':      THEME.__THEME_WARNING_COLOR__,
        'warning-700':      Color(THEME.__THEME_WARNING_COLOR__).darken(0.2).hex(),
        error:              THEME.__THEME_ERROR_COLOR__,
        'error-100':        Color(THEME.__THEME_ERROR_COLOR__).lighten(0.4).hex(),
        'error-200':        Color(THEME.__THEME_ERROR_COLOR__).lighten(0.3).hex(),
        'error-300':        Color(THEME.__THEME_ERROR_COLOR__).lighten(0.2).hex(),
        'error-400':        Color(THEME.__THEME_ERROR_COLOR__).lighten(0.1).hex(),
        'error-500':        THEME.__THEME_ERROR_COLOR__,
        'error-700':        Color(THEME.__THEME_ERROR_COLOR__).darken(0.2).hex(),
        light:              THEME.__THEME_LIGHT_COLOR__,
        dark:               THEME.__THEME_DARK_COLOR__,
        'dark-2':           THEME.__THEME_DARK_2_COLOR__,
        'dark-3':           THEME.__THEME_DARK_3_COLOR__
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: [
        'disabled'
      ],
      borderWidth: [
        'hover'
      ],
      opacity: [
        'disabled'
      ],
      pointerEvents: [
        'disabled'
      ],
      textColor: [
        'disabled'
      ],
      fill: [
        'dark'
      ]
    }
  },
  plugins: []
}
